package worms.model;

public enum GameState {
	INITIALISING,
	PLAYING
}