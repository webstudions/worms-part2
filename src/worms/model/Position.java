package worms.model;

import worms.util.Util;
import be.kuleuven.cs.som.annotate.*;

@Value
public class Position implements Cloneable {
	
	/**
	 * Initializing this position with given x and y coordinate.
	 * @param 	x
	 * 		  	The x coordinate for this GameObject in meters.
	 * @param 	y
	 * 		  	The y coordinate for this GameObject in meters.
	 * @throws 	| IllegalArgumentException
	 * 			| 	Double.isNaN(x) || Double.isNaN(y)
	 */
	@Raw
	public Position(Double x, Double y) throws IllegalArgumentException {
		if(Double.isNaN(x) || Double.isNaN(y))
			throw new IllegalArgumentException();
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return	Return the x coordinate of this position.
	 * 			| result == this.x
	 */
	@Basic @Immutable
	public Double getX() {
		return this.x;
	}
	
	private final Double x;
	
	/**
	 * @return	Return the y coordinate of this position.
	 * 			| result == this.y
	 */
	@Basic @Immutable
	public Double getY() {
		return this.y;
	}
	
	private final Double y;
	
	/**
	 * Returns a Position that is identical to this position.
	 * @return	new Position(this.getX(), this.getY());
	 */
	@Override
	public Position clone() {
    	try {
			return (Position) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
    }
	
	/**
	 * Returns whether this position is equal to the given position.
	 * @param 	position
	 * 			The position object that we use to compare with this position.
	 * @return	Returns whether this position is equal to the given position.
	 * 			| (this.getX() == position.getX()) && (this.getY() == position.getY());
	 */
	public boolean isEqual(Position position) {
		return (Util.fuzzyEquals(this.getX(), position.getX()) && Util.fuzzyEquals(this.getY(), position.getY())) ;
	}
}
