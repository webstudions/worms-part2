package worms.model;

import java.util.ArrayList;
import java.util.Random;

import be.kuleuven.cs.som.annotate.*;
import worms.util.Util;

/**
 * @invar	The gameState of this world is always valid.
 * 			| isValidGameState(this.getGamestate())
 * @invar	The world contains maximum 10 teams
 * 			| this.getTeams().size() <= 10
 * @author 	nickscheynen
 * @author 	carmenvandeloo
 *
 */
public class World {
	/**
	 * Creates a new world.
	 * @param 	width 
	 * 			The width of the world (in meter)
	 * @param 	height 
	 * 			The height of the world (in meter)
	 * @param 	passableMap 
	 * 			A rectangular matrix indicating which parts of the terrain are passable and impassable.     
	 * @param 	random 
	 * 			A random number generator.
	 * @post	The width of this World is equal to the given width.
	 * 			| new.getWidth() == width
	 * @post	The height of this World is equal to the given height.
	 * 			| new.getHeight() == height
	 * @post	The passable map of this world is equal to the given passable map.
	 * 			| new.getPassableMap() == passableMap
	 * @post 	The number of pixels per meter in width is equal to the width of the passable map divided by the width of the map.
	 * 			| new.PPMHWidth == this.passableMap[0].length/this.getWidth();
	 * @post 	The number of pixels per meter in height is equal to the height of the passable map divided by the height of the map.
	 * 			| new.PPMHHeight == this.passableMap.length/this.getHeight();
	 * @post 	The number of meter per pixel is equal to the height of the map divided by the height of the passable map.
	 * 			| new.MPP == this.getHeight()/this.passableMap.length;
	 * @post	The Gamestate is set to INITIALISING
	 * 			| new.GameState == INITIALISING;
	 * @throws	IllegalArgumentException
	 * 			| width<0 || Double.isNaN(width) || height < 0 || Double.isNaN(height)
	 */
	@Raw
	public World(double width, double height, boolean[][] passableMap, Random random) throws IllegalArgumentException {
		if(width<0 || Double.isNaN(width) || height < 0 || Double.isNaN(height))
			throw new IllegalArgumentException();
		this.width = width;
		this.height = height;
		this.passableMap = passableMap;
		this.PPMWidth = this.passableMap[0].length/this.getWidth();
		this.PPMHeight = this.passableMap.length/this.getHeight();
		this.MPP = this.getHeight()/this.passableMap.length;
		this.randomGenerator = random;
		this.gameState = GameState.INITIALISING;
	}
	
	/**
	 * Return the width of this world.
	 */
	public double getWidth() {
		return width;
	}
	
	private final double width;
	
	/**
	 * Return the height of this world.
	 */
	public double getHeight() {
		return height;
	}
	
	private final double height;
	
	/**
	 * Return the passableMap of this world.
	 */
	public boolean[][] getPassableMap() {
		return passableMap;
	}
	
	private final boolean[][] passableMap;
	
	/**
	 * Return the random number generator of this world.
	 */
	public Random getRandomGenerator() {
		return randomGenerator;
	}
	
	private final Random randomGenerator;
	
	/**
	 * Return the number of pixels per meter in height of this world.
	 */
	public final double getPPMHeight() {
		return this.PPMHeight;
	}
	
	private final double PPMHeight;

	/**
	 * Return the number of pixels per meter in width of this world.
	 */
	public final double getPPMWidth() {
		return this.PPMWidth;
	}
	
	private final double PPMWidth;
	
	/**
	 * Returns the number of meter per pixel in this world.
	 */
	protected double getMPP() {
		return this.MPP;
	}
	
	private final double MPP; 
	
	
	/**
	 * Create and add an empty team with the given name to the given world.
	 * @param	newName 
	 * 			| name of the team
	 * @post	If a team with the given name doesn't exist already, a new team is added and made the team currently being added. 
	 * 			If it exists no team is added and the team that is being added is set to the team found with this name.
	 * 			| Team teamWithName = getTeamWithName(newName);
	 * 			| if(teamWithName == null) {
	 * 			|	Team newTeam = new Team(this, newName);
	 * 			|	this.getTeams().add(newTeam);
	 *			|	this.setAddingTeam(newTeam);
	 * 			|} else {
	 * 			|	this.setAddingTeam( teamWithName );
	 * 			|}
	 * @throws	IllegalArgumentException
	 * 			If the creation of a new team throws an IllegalArgumentException.
	 * @throws	UnsupportedOperationException
	 * 			| this.getTeams().size()==10
	 */
	public void addEmptyTeam(String newName) {
		if(this.getTeams().size()==10)
			throw new UnsupportedOperationException();
		if (this.getGameState() == GameState.INITIALISING) {
			Team teamWithName = getTeamWithName(newName);	
			if(teamWithName == null) {
				try {
					Team newTeam = new Team(this, newName);
					this.getTeams().add(newTeam);
					this.setAddingTeam(newTeam);
				} catch(IllegalArgumentException exc) {
					throw new IllegalArgumentException(exc);
				}
			} else {
				this.setAddingTeam( teamWithName );
			}
		}
	};
	
	/**
	 * Returns an ArrayList with all the teams in the given world
	 */
	protected ArrayList<Team> getTeams() {
		return this.worldTeams;
	};
	
	/**
	 * Checks if a team with the given name exists and if so returns the team object.
	 * @param 	name 
	 * 			The name of the team.
	 * @return	Returns the team object if a team with the given name exists and null if it doesn't.
	 * 			| if(worldTeams.get(i).getName().equals(name)) { return worldTeams.get(i); }
	 */
	private Team getTeamWithName(String name) {
		ArrayList<Team> worldTeams = this.getTeams();
		for (int i = 0; i < worldTeams.size(); i++) {
			if(worldTeams.get(i).getName().equals(name)) {
				return worldTeams.get(i);
			}
		}
		return null;
	}
	
	private ArrayList<Team> worldTeams = new ArrayList<Team>();
	
	/**
	 * Sets the team that is currently being added to the world.
	 * @param 	team 
	 * 			Team object.
	 * @post	The team that is currently being added to the world is the given team.
	 * 			| this.getAddingTeam() == team
	 */
	private void setAddingTeam(Team team) {
		this.addingTeam = team;
	}
	
	/**
	 * Returns the team that is currently being added to the world.
	 */
	private Team getAddingTeam() {
		return this.addingTeam;
	}
	
	private Team addingTeam = null;

	/**
	 * Create and add a new food ration to a random position of the given world.
	 * @post	A new food ration is created and added to the worldFood list.
	 * 		 	| Position position = GetValidRandomPosition(Food.getFoodRadius());
	 *			| Food newFood = new Food(this,position.getX(),position.getY());
	 *			| worldFood.add(newFood);
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */	
	public void addNewFood() throws IllegalArgumentException {
		if (this.getGameState() == GameState.INITIALISING) {
			try {
				Position position = GetValidRandomPosition(Food.getFoodRadius());
				Food newFood = new Food(this,position.getX(),position.getY());
				worldFood.add(newFood);
			} catch(IllegalArgumentException exc) {
				throw new IllegalArgumentException();
			}
		}
	}
	
	/**
	 * Create a food ration that is positioned at the given location in the given world.
	 * @param 	x
	 * 			The x position in meters.
	 * @param 	y
	 * 			The y position in meters.
	 * @param	world
	 * 			The world in which to create the new food ration.
	 * @return 	The created food ration.
	 * @throws 	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */
	public Food addNewCreatedFood(double x, double y) throws IllegalArgumentException {
		try {
			if ( (this.getGameState() == GameState.INITIALISING) ) {
				Food newFood = new Food(this, x, y);
				this.getFood().add(newFood);
				return newFood;
			} else
				return null;
			}
		catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Removes the given food ration from the world.
	 * @param 	food
	 * 			The food object to be removed.
	 * @post	The food rations active state is set to false.
	 * 			| food.getActive() == false
	 * @post	The food ration is removed from the worldFood arraylist.
	 * 			| new.getFood().contains(food) == false
	 */
	public void removeFood(Food food) {
		food.DIE();
		this.getFood().remove(food);
	}
	
	/**
	 * Returns all the food rations in the world as an ArrayList.
	 */
	public ArrayList<Food> getFood() {
		return this.worldFood;
	};
	
	private ArrayList<Food> worldFood = new ArrayList<Food>();
	
	/**
	 * Create and add a new worm to the given world at a random location, with a direction of 0.2 and a direction of 0.3, as name "Worm"+index and a team if there is an addingTeam.
	 * @effect	The position of the new worm is calculated using the GetValidRandomPosition() method.
	 * 			| Position position = GetValidRandomPosition(0.3);
	 * @effect	The team of the worm is set using the setTeam() method of worm.
	 * 			| newWorm.setTeam(this.getAddingTeam());
	 * @effect	The worm is added to both the worldWorms and activeWorldWorms arrayLists using the appropriate setters.
	 * 			| this.getWorms().add(newWorm);
	 * 			| this.getActiveWorms().add(newWorm);
	 * @effect	The addingTeam is reset to null using the setAddingTeam() method.
	 * 			| this.setAddingTeam(null);
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException ) 
	 */
	public void addNewWorm() throws IllegalArgumentException {
		if (this.getGameState() == GameState.INITIALISING) {
		try {
			Position position = GetValidRandomPosition(0.35);
			Worm newWorm = new Worm(this,position.getX(),position.getY(), 4.5, 0.35, "Worm "+(this.getWorms().size()+1));
			newWorm.setTeam(this.getAddingTeam());
			this.getWorms().add(newWorm);
			this.getActiveWorms().add(newWorm);
			this.setAddingTeam(null);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		} }
	};
	
	/**
	 * Creates (and adds) a new worm that is positioned at the given location in this world, looks in the given direction, has the given radius and the given name.
	 * The new worm may (but isn't required to) have joined a team.
	 * @param 	x
	 * 			The x-coordinate of the position of the new worm (in meter)
	 * @param 	y
	 * 			The y-coordinate of the position of the new worm (in meter)
	 * @param 	direction
	 * 			The direction of the new worm (in radians)
	 * @param 	radius 
	 * 			The radius of the new worm (in meter)
	 * @param 	name
	 * 			The name of the new worm
	 * @throws	UnsupportedOperationException
	 * 			| !this.getGameState() == GameState.INITIALISING
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */
	public Worm addNewCreatedWorm(double x, double y, double direction, double radius, String name) throws UnsupportedOperationException, IllegalArgumentException {
		if (! (this.getGameState() == GameState.INITIALISING) ) 
			throw new UnsupportedOperationException();
		try {
			Worm newWorm = new Worm(this, x, y, direction, radius, name);
			newWorm.setTeam(this.getAddingTeam());
			this.getWorms().add(newWorm);
			this.getActiveWorms().add(newWorm);
			this.setAddingTeam(null);
			return newWorm;
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
			
	}
	
	/**
	 * Sets the active state of the given worm to false, sets the active projectile to null and removes the worm from the activeWorldWorms arraylist.
	 * @param 	worm
	 * 			Worm object to be removed.
	 * @effect	The worm's active state is set to false using the DIE() method.
	 * 			| worm.DIE()
	 * @effect	The world's active projectile is set to null using the setActiveProjectile() method
	 * 			| this.setActiveProjectile(null);
	 * @post	The worm is removed from the world's activeWorldWorms arraylist.
	 * 			| !this.getActiveWorms().contains(worm);
	 */
	public void removeWorm(Worm worm) {
		worm.DIE();
		this.setActiveProjectile(null);
		this.getActiveWorms().remove(worm);
	}
	
	/**
	 * Returns all the worms in the given world as an arraylist.
	 */
	public ArrayList<Worm> getWorms() {
		return this.worldWorms;
	};
	
	/**
	 * Returns all the active worms in the given world
	 */
	public ArrayList<Worm> getActiveWorms() {
		return this.activeWorldWorms;
	};
	
	private ArrayList<Worm> worldWorms = new ArrayList<Worm>();
	private ArrayList<Worm> activeWorldWorms = new ArrayList<Worm>();

	/**
	 * Sets the world's gamestate to the given gamestate
	 * @param 	gamestate
	 * 			The gamestate to be set.
	 * @throws 	IllegalArgumentException
	 * 			| !isValidGameState(gamestate)
	 */
	@Raw
	private void setGameState(GameState gamestate) throws IllegalArgumentException {
		if(!isValidGameState(gamestate))
			throw new IllegalArgumentException();
		this.gameState = gamestate;
	}
	
	/**
	 * Gets the gamestate of this world.
	 */
	public GameState getGameState() {
		return this.gameState;
	}
	
	/**
	 * Checks if the given gamestate is a valid gamestate.
	 * @param 	gamestate
	 * 			The given gamestate.
	 * @return 	Returns true if the gamestate is valid.
	 * 			| return (gameState == GameState.INITIALISING) || (gameState == GameState.PLAYING);
	 */
	private boolean isValidGameState(GameState gamestate) {
		return (gameState == GameState.INITIALISING) || (gameState == GameState.PLAYING);
	}
	
	private GameState gameState;
	
	/**
	 * Returns a valid random position given the radius of the radius of the GameObject.
	 * @param 	radius
	 * 			The radius of the GameObject for which we need a valid random position.
	 * @return	Returns a valid random position given the radius of the radius of the GameObject.
	 * 			| Position randomPerimeterPosition =  getRandomPerimeterPosition();
	 * 			| Position validPosition = PerimeterPositionToValidPosition( randomPerimeterPosition , radius);
	 * 			| return validPosition;
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(radius)
	 */
	private Position GetValidRandomPosition(double radius) throws IllegalArgumentException {
		if(Double.isNaN(radius)) 
			throw new IllegalArgumentException();
		Position randomPerimeterPosition =  getRandomPerimeterPosition();
		Position validPosition = PerimeterPositionToValidPosition( randomPerimeterPosition , radius);
		return validPosition;
		
	}
	
	/**
	 * Returns a random position on the perimeter of the world.
	 * @return	Returns a random position on the perimeter of the world.
	 * 			| if(perimeterLocation < this.getHeight()) return new Position(0.0, perimeterLocation);
	 * 			| if( (perimeterLocation >= this.getHeight()) && (perimeterLocation < this.getHeight()+this.getWidth()) ) return new Position(perimeterLocation-this.getHeight(), this.getHeight());
	 * 			| if( (perimeterLocation >= this.getHeight()+this.getWidth()) && (perimeterLocation < this.getHeight()*2 + this.getWidth())) return new Position(this.getWidth(), this.getHeight() - (perimeterLocation-this.getHeight()-this.getWidth()));
	 * 			| else return new Position(this.getWidth() - (perimeterLocation-2*this.getHeight()-this.getWidth()), 0.0);
	 */
	private Position getRandomPerimeterPosition() {
		double maxLength = 2 * this.getWidth() + 2 * this.getHeight();
	
		double perimeterLocation = maxLength * this.randomGenerator.nextDouble();
		
		if(perimeterLocation < this.getHeight()) {
			return new Position(0.0, perimeterLocation); 
		} else if( (perimeterLocation >= this.getHeight()) && (perimeterLocation < this.getHeight()+this.getWidth()) ) {
			return new Position(perimeterLocation-this.getHeight(), this.getHeight());
		} else if( (perimeterLocation >= this.getHeight()+this.getWidth()) && (perimeterLocation < this.getHeight()*2 + this.getWidth())) {
			return new Position(this.getWidth(), this.getHeight() - (perimeterLocation-this.getHeight()-this.getWidth()));
		} else {
			return new Position(this.getWidth() - (perimeterLocation-2*this.getHeight()-this.getWidth()), 0.0);
		}
	}
	
	/**
	 * Converts to given perimetPosition
	 * @param 	pPosition
	 * 			Position on the perimeter of the world.
	 * @param 	radius
	 * 			Radius of the GameObject to be placed in the world.
	 * @return	The method checks where the perimeter position is compared to the center of the map and then checks for a valid position in the direction of the center, returning the first one found.
	 * 			| while( (!isAdjacent(tryPosition.getX(), tryPosition.getY(), radius)) && !isOutOfWorld(tryPosition) ) {
	 *			| 	if(Util.fuzzyLessThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyLessThanOrEqualTo(pPosition.getY(), centerY)) {
	 *			|		tryPosition = new Position( (tryPosition.getX() + cosAngle * 0.1 * radius) , (tryPosition.getY() + sinAngle * 0.1 * radius) );
	 *			|	} else if(Util.fuzzyLessThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyGreaterThanOrEqualTo(pPosition.getY(), centerY)) {
	 *			|		tryPosition = new Position( (tryPosition.getX() + cosAngle * 0.1 * radius) , (tryPosition.getY() - sinAngle * 0.1 * radius) );
	 *			|	} else if(Util.fuzzyGreaterThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyGreaterThanOrEqualTo(pPosition.getY(), centerY)) {
	 *			|		tryPosition = new Position( (tryPosition.getX() - cosAngle * 0.1 * radius) , (tryPosition.getY() - sinAngle * 0.1 * radius) );
	 *			|	} else if(Util.fuzzyGreaterThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyLessThanOrEqualTo(pPosition.getY(), centerY)) {
	 *			| 		tryPosition = new Position( (tryPosition.getX() - cosAngle * 0.1 * radius) , (tryPosition.getY() + sinAngle * 0.1 * radius) );
	 *			| 	} else {
	 *			|		return null;
	 *			|	}
	 *			| }
	 *			| return tryPosition;
	 */
	private Position PerimeterPositionToValidPosition(Position pPosition, double radius) {
		double centerX = this.getWidth()/2.0; 
		double centerY = this.getHeight()/2.0;
		//System.out.println("Center: x ="+centerX+"; y ="+centerY);
		double dX = Math.abs(centerX - pPosition.getX());
		double dY = Math.abs(centerY - pPosition.getY());
		//System.out.println("delta: x ="+dX+"; y ="+dY);
		double angle = Math.asin( (dY) / Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2) ) );

		double sinAngle = Math.sin( angle );
		double cosAngle = Math.cos( angle );	
		
		Position tryPosition = new Position( pPosition.getX(), pPosition.getY());
		while( (!isAdjacent(tryPosition.getX(), tryPosition.getY(), radius)) && !isOutOfWorld(tryPosition) ) {
			if(Util.fuzzyLessThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyLessThanOrEqualTo(pPosition.getY(), centerY)) {
				tryPosition = new Position( (tryPosition.getX() + cosAngle * 0.01 * radius) , (tryPosition.getY() + sinAngle * 0.1 * radius) );
			} else if(Util.fuzzyLessThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyGreaterThanOrEqualTo(pPosition.getY(), centerY)) {
				tryPosition = new Position( (tryPosition.getX() + cosAngle * 0.01 * radius) , (tryPosition.getY() - sinAngle * 0.1 * radius) );
			} else if(Util.fuzzyGreaterThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyGreaterThanOrEqualTo(pPosition.getY(), centerY)) {
				tryPosition = new Position( (tryPosition.getX() - cosAngle * 0.01 * radius) , (tryPosition.getY() - sinAngle * 0.1 * radius) );
			} else if(Util.fuzzyGreaterThanOrEqualTo(pPosition.getX(), centerX) && Util.fuzzyLessThanOrEqualTo(pPosition.getY(), centerY)) {
				tryPosition = new Position( (tryPosition.getX() - cosAngle * 0.01 * radius) , (tryPosition.getY() + sinAngle * 0.1 * radius) );
			} else {
				return null;
			}
		}
		return tryPosition;
	}
	
	/**
	 * Returns true if the given position lies outside the boundaries of the world.
	 * @param 	position
	 * 			The position to check.
	 * @return	Returns true if the given position lies outside the boundaries of the world.
	 * 			| result == position.getX() < 0.0 || position.getX() > this.getWidth() || position.getY() < 0.0 || position.getY() > this.getHeight();
	 */
	public boolean isOutOfWorld(Position position) {
		return position.getX() < 0.0 || position.getX() > this.getWidth() 
			|| position.getY() < 0.0 || position.getY() > this.getHeight(); 
	}
	
	/**
	 * Checks whether the given circular region of this world, defined by the given center coordinates and radius, is passable and adjacent to impassable terrain. 
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return 	True if the given region is passable and adjacent to impassable terrain, false otherwise.
	 * 			| return (!isImpassable(x, y, radius) && closeToImpassable(x, y, radius));
	 */
	public boolean isAdjacent(double x, double y, double radius) {
		return (!isImpassable(x, y, radius) && closeToImpassable(x, y, radius));
	};
	
	/**
	 * Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * The method will make sure that if the object's radius only 1px, it will check the minimum radius for at least one pixel outside the radius.  
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return	The method will make sure that if the object's radius only 1px, it will check the minimum radius for at least one pixel outside the radius. 	
	 * 			Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * 			| if(intRadius<=1) {
	 *			|	return isImpassable(x, y, 2.0/(this.getPPMWidth()/1.1));
	 *			|  } 
	 * @return 	Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * 			| return isImpassable(x, y, 1.1 * radius);
	 */
	public boolean closeToImpassable(double x, double y, double radius) {
		int intRadius = convertMeterToPixelRadius(radius);
		if (intRadius == Integer.MAX_VALUE) return false;
		
		int border = this.convertMeterToPixelRadius(radius * 1.1); 
		
		if(border<intRadius + 1) {
			return isImpassable(x, y, radius + this.getMPP());
		}
		
		return isImpassable(x, y, 1.1 * radius);
	}
	
	/**
	 * Checks whether the given circular region of the given world, defined by the given center coordinates and radius, is impassable. 
	 * @param 	world 
	 * 			The world in which to check impassability 
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return	Check only the object's pixel if the radius is smaller than one pixel and returns true if the given region is impassable, false otherwise
	 * 			| if(intRadius<=1) { if(passableMap[intY][intX] == false) { return true; } else { return false; } }
	 * @return 	Uses the Midpoint circle algorithm (Bresenham's circle algortihm, http://en.wikipedia.org/wiki/Midpoint_circle_algorithm#Optimization).
	 * 			We adapted the snippet found on http://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#Java to find the pixels that need to be checked.  
	 * 			Returns true if the given region is impassable, false otherwise.
	 * 			| do {
	 * 			| 	for(j=intY-y;j<=intY+y;j++) { if(passableMap[j][intX+x] == false) return true; }
	 * 			| 	for(j=intY-y;j<=intY+y;j++) { if(passableMap[j][intX-x] == false) return true; }
	 * 			| 	for(j=intY-x;j<=intY+x;j++) { if(passableMap[j][intX+y] == false) return true; }
	 * 			| 	for(j=intY-x;j<=intY+x;j++) { if(passableMap[j][intX-y] == false) return true; }
	 * 			| 	if (d < 0) {
	 * 			| 		d += 2 * x + 1;
	 * 			| 	} else {
	 * 			| 		d += 2 * (x - y) + 1;
	 * 			| 		y--;
	 * 			| 	}
	 * 			| 	x++;
	 * 			| } while(x <= y);
	 */
	
	public boolean isImpassable(double centerX, double centerY, double radius) {

		if(Double.isNaN(centerX) && Double.isNaN(centerY) && Double.isNaN(radius)) {
			return false;
		}
		
		int intX = convertMeterToPixelX(centerX);
		if (intX == Integer.MAX_VALUE) return false;
		
		int intY = convertMeterToPixelY(centerY);
		if (intY == Integer.MAX_VALUE) return false;
		
		int intRadius = convertMeterToPixelRadius(radius);
		if (intRadius == Integer.MAX_VALUE) return false;
		
		if( intX+intRadius < this.getWidth()*this.getPPMWidth() && intX-intRadius >= 0.0 
				&& intY+intRadius < this.getHeight()*this.getPPMHeight() && intY-intRadius >= 0.0				
		) {
			
			if(intRadius<=1) {
				if(passableMap[intY][intX] == false) { return true; } else { return false; }
			}
			
			int d = (int) Math.round( (5.0 - intRadius * 4.0)/4.0 );
			
			int x = 0;
			int y = intRadius;
			int j;
			
			do {
				for(j=intY-y;j<=intY+y;j++) { if(passableMap[j][intX+x] == false) return true; }
				
				for(j=intY-y;j<=intY+y;j++) { if(passableMap[j][intX-x] == false) return true; }
				
				for(j=intY-x;j<=intY+x;j++) { if(passableMap[j][intX+y] == false) return true; }

				for(j=intY-x;j<=intY+x;j++) { if(passableMap[j][intX-y] == false) return true; }
				
				if (d < 0) {
					d += 2 * x + 1;
				} else {
					d += 2 * (x - y) + 1;
					y--;
				}
				x++;
				
			} while(x <= y);
		}
		return false;
	}
	
	/**
	 * Returns the pixel x position for the given meter x position.
	 * @param 	xMeter
	 * 			The given meter x position to convert.
	 * @return	The converted meter x position in pixels.
	 * 			| result == longIntX = Math.round(Math.floor( xMeter * this.getPPMHeight() ));
	 */
	private int convertMeterToPixelX(double xMeter) {
		long longIntX = Math.round(Math.floor( xMeter * this.getPPMHeight() ));
		if (longIntX >= Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		} else {
			return (int) longIntX;
		}
	}

	/**
	 * Returns the pixel y position for the given meter y position.
	 * @param 	yMeter
	 * 			The given meter y position to convert.
	 * @return	The converted meter y position in pixels.
	 * 			| result == longIntY = Math.round(Math.floor( yMeter * this.getPPMHeight() ));
	 */
	private int convertMeterToPixelY(double yMeter) {
		long longIntY = Math.round(Math.floor( (this.getHeight() - yMeter) * this.getPPMHeight() ));
		if (longIntY >= Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		} else {
			return (int) longIntY; 
		}
	}
	
	/**
	 * Returns the pixel y position for the given meter y position.
	 * @param 	yMeter
	 * 			The given meter y position to convert.
	 * @return	The converted meter y position in pixels.
	 * 			| result == longIntY = Math.round(Math.floor( yMeter * this.getPPMHeight() ));
	 */
	private int convertMeterToPixelRadius(double radius) {
		long longIntRadius = Math.round(Math.ceil( radius * this.getPPMHeight() ));
		if (longIntRadius >= Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		} else {
			return (int) longIntRadius; 
		}
	}

	/**
	 * Returns the active worm in the given world (i.e., the worm whose turn it is).
	 * @return	Returns the active worm in the given world (i.e., the worm whose turn it is).
	 * 			| for (Worm worm : this.getWorms()) { if(worm.getIsSelected())  { return worm; } } return null; }
	 */
	public Worm getCurrentWorm() {
		for (Worm worm : this.getWorms())
			if(worm.getIsSelected()) 
				return worm;
		return null;
	}
	
	/**
	 * Returns the index of the next worm in the arraylist.
	 * @return	Returns the index of the current worm + 1 or 0 if the current worm is the last worm of the list.
	 * 			| if(this.getWorms().get(i).getIsSelected()) {
	 * 			| 	if(i == this.getWorms().size()-1) return 0;
	 * 			| 	else return i+1;
	 * 			| }
	 */
	private int getIndexNextWorm() {
		for (int i = 0; i < this.getWorms().size(); i++) {
			if(this.getWorms().get(i).getIsSelected()) {
				if(i == this.getWorms().size()-1)
					return 0;
				else 
					return i+1;
			} 
		}
		return 0;		
	}
	
	
	/**
	 * Starts a game in the given world.
	 */
	public void startGame(){
		if (this.getNbOfWorms() != 0) {
			this.setGameState(GameState.PLAYING);
			this.getWorms().get(0).setIsSelected(true);
			this.getWorms().get(0).getWormProjectiles().get(0).activate();
		}
	}
	
	private int getNbOfWorms () {
		return this.getWorms().size();
	}
	
	/**
	 * Starts the next turn in the given world. Deselects the current worm, selects the next active worm, 
	 * deactivates the projectile of the current worm and activates the first projectile of the next active worm.
	 * @effect	The selected weapon of the current worm is deactivated using the deactivate method.
	 * 			| this.getCurrentWorm().getSelectedWeapon().deactivate();
	 * @effect	The current worm is deselected using the setIsSelected() method.
	 * 			| this.getCurrentWorm().setIsSelected(false);
	 * @effect	The next active worm's action points are set to the maximum action points using the setActionPoints() method.
	 * 			| nextWorm.setActionPoints(nextWorm.getMaximumActionPoints());
	 * @effect	The next active worm's hitpoints are increased with 10, using the setHitPoints() method;
	 * 			| nextWorm.setHitPoints(nextWorm.getHitPoints()+10);
	 * @effect 	The next active worm's state is set to selected, using the setIsSelected() method.
	 * 			| nextWorm.setIsSelected(true);
	 * @effect	The next active worm's first projectile is activated using the activate() method.
	 * 			| nextWorm.getWormProjectiles().get(0).activate();
	 */
	public void startNextTurn() {
		Worm nextWorm = this.getWorms().get(this.getIndexNextWorm());
		if(this.getCurrentWorm().isActive()) {
			this.getCurrentWorm().getSelectedWeapon().deactivate();
		}
		this.getCurrentWorm().setIsSelected(false);
		if (nextWorm.isActive() == false) {
			nextWorm.setIsSelected(true);
			this.startNextTurn();
		} else {
			nextWorm.setActionPoints(nextWorm.getMaximumActionPoints());
			nextWorm.setHitPoints(nextWorm.getHitPoints()+10);
			nextWorm.setIsSelected(true);
			nextWorm.getWormProjectiles().get(0).activate();
		}
	};
	
	/**
	 * Returns the name of a single worm if that worm is the winner, or the name of a team if that team is the winner. This method should null if there is no winner.
	 * @return	Returns the name of a single worm if that worm is the winner, or the name of a team if that team is the winner. This method returns null if there is no winner.
	 * 			| if (activeWorms.size() == 1) { 
	 * 			|	if ( activeWorms.get(0).getTeamName() == null ) {
	 * 			|		return activeWorms.get(0).getName();
	 * 			|	} else {
	 * 			|		 return activeWorms.get(0).getTeamName();
	 * 			|	}
	 * 			| } else {
	 * 			| 	int nbParties = 0;
	 * 			|	ArrayList<Team> winnableTeamList = new ArrayList<Team>();
	 * 			|	for(int i = 0; i<activeWorms.size();i++) {
	 * 			|		if(activeWorms.get(i).getTeam() == null) {
	 * 			|			nbParties=nbParties+1;
	 * 			|		} else {
	 * 			|			if(!winnableTeamList.contains(activeWorms.get(i).getTeam())) {
	 * 			|				winnableTeamList.add(activeWorms.get(i).getTeam());
	 * 			|				nbParties= nbParties+1;
	 * 			|			}
	 * 			|		}
	 * 			|	}
	 * 			|	if(nbParties==1) {
	 * 			|		this.setGameState(GameState.TERMINATED);
	 * 			|		return winnableTeamList.get(0).getName();
	 * 			|	} else {
	 * 			|		return null;
	 * 			|	}
	 * 			| }
	 */
	public String getWinner() {
		if (this.getGameState() == GameState.PLAYING) {
			ArrayList<Worm> activeWorms = this.getActiveWorms();
			//no active worms.
			if (activeWorms.size() == 0) {
				return null;
			}
			//only one active worm.
			if (activeWorms.size() == 1) {
				if ( activeWorms.get(0).getTeamName() == null ) {
					return activeWorms.get(0).getName();
				}
				else return activeWorms.get(0).getTeamName();
			}
			//multiple active worms.
			else {
				int nbParties = 0;
				ArrayList<Team> winnableTeamList = new ArrayList<Team>();
				for(int i = 0; i<activeWorms.size();i++) {
					if(activeWorms.get(i).getTeam() == null) {
						nbParties=nbParties+1;
					} else {
						if(!winnableTeamList.contains(activeWorms.get(i).getTeam())) {
							winnableTeamList.add(activeWorms.get(i).getTeam());
							nbParties= nbParties+1;
						}
					}
				}
				if(nbParties==1) {
					return winnableTeamList.get(0).getName();
				} else {
					return null;
				}
			}
		}
		//if gamestate is not PLAYING.
		return null;
	};
	
	
	/**
	 * Returns whether the game in the given world has finished.
	 * @return	Returns whether the game in the given world has finished.
	 * 			| if (this.getWinner() == null) { 
	 * 			|	if(this.getActiveWorms().size()==0) { return true; } else { return false; }
	 * 			| } else return true;
	 */
	public boolean isGameFinished() {
		if (this.getWinner() == null) {
			if(this.getActiveWorms().size()==0) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	};
	
	/**
	 * Makes the given projectile the active projectile of this world.
	 * @param 	projectile
	 * 			The projectile to make the active projectile of this world.
	 * @post	this.getActiveProjectile() == projectile
	 */
	public void setActiveProjectile(Projectile projectile) {
		this.activeProjectile = projectile;
	}
	
	/**
	 * Returns the active projectile in the world, or null if no active projectile exists.
	 */
	public Projectile getActiveProjectile() {
		return this.activeProjectile;
	};
	
	private Projectile activeProjectile;
	
};